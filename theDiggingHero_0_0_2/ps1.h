#ifndef PS1_H
#define PS1_H

#include <QMainWindow>
#include<npc.h>
#include<player.h>
#include<soil.h>
#include<equipt.h>

namespace Ui {
class ps1;
}

class ps1 : public QMainWindow
{
    Q_OBJECT

public:
    explicit ps1(QWidget *parent = 0);
    ~ps1();
    Player *myself;
    soil *tudi[20][20];
void keyPressEvent(QKeyEvent *ev);
  int score;
  void paintSky();
  void paintBackofSoil();
  void paintBloodBar();
void paintSoilandMine();//绘制挖的矿
NPC *Tutu;
QLabel* scoreLabel;
void paintEvent(QPaintEvent* ev);
void keyReleaseEvent(QKeyEvent *ev);
void movingNPC();
int t;

int ans;

private:
    Ui::ps1 *ui;
};

#endif // PS1_H
