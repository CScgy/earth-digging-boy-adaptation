#ifndef SOIL_H
#define SOIL_H

#include <QWidget>
#include<QPushButton>
class soil : public QPushButton
{
    Q_OBJECT
public:
    explicit soil(QWidget *parent = nullptr);
    soil(QString soilImg);
    void changeCloth(QString soilImg);
    int val;
    int posX;
    int posY;
    void deleteit();
    QString soilImg;

signals:

};

#endif // SOIL_H
