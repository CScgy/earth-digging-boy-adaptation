#ifndef PS3_H
#define PS3_H

#include<ps1.h>
#include<QMainWindow>
class ps3:public ps1
{
public:
    ps3();
    void paintEvent(QPaintEvent *ev);
    void paintSoilandMine();
    void createNPC();
    void nextlevel();
QMainWindow *next;
};

#endif // PS3_H
