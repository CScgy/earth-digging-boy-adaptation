QT       += core gui
QT+=multimedia
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11
CONFIG += resources_big
# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    equipt.cpp \
    main.cpp \
    mypushbutton.cpp \
    npc.cpp \
    player.cpp \
    playscene.cpp \
    soil.cpp \
    startui.cpp \
    ps1.cpp \
    ps3.cpp \
    ps4.cpp

HEADERS += \
    equipt.h \
    mypushbutton.h \
    npc.h \
    player.h \
    playscene.h \
    soil.h \
    startui.h \
    ps1.h \
    ps3.h \
    ps4.h

FORMS += \
    playscene.ui \
    startui.ui \
    ps1.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res.qrc
